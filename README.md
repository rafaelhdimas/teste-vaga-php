# Teste prático

Eu decidi desenvolver o projeto sem separar entre Api Restfull e Cliente consumindo a Api por ser um blog, e não ter tanta interação no frontend que justifique a utilização de um framework javascript para montar a interface do usuário.

## Instalação
  - As instruções abaixo, considera que o docker e o docker-compose já está instalado, e que o repositório já foi clonado:
    - Execute o seguinte comando para subir os container docker:
      - ```bash docker-compose up```
    - Instale as dependencias:
      - ``` bash docker exec -it netzee-app composer install```
    - Gere o simbólico de "public/storage" para "storage/app/public":
      - ```bash docker exec -it netzee-app php artisan storage:link ```
    - Rode as migrations executando os seeds:
      - ```bash docker exec -it netzee-app php artisan migrate --seed ```

## Utilização
O Blog roda em localhost (porta 80), e para acessar o dashboard é preciso efetuar o login dos os dados gerados pelos seeds.
Todos os usuarios gerados pelos seeds a senha é 'secret', rodando o seguinte comando você acessa o tinke e 
consegue recuperar os emails.

```bash
docker exec -it netzee-app php artisan tinke
```
