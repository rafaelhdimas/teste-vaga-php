const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/blog/app.js', 'public/blog/js')
   .sass('resources/sass/blog/app.scss', 'public/blog/css');

mix.js('resources/js/dashboard/app.js', 'public/dashboard/js');
