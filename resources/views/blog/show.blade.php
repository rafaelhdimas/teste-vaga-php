@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <img class="img-fluid" src="{{ $post->coverUrl() }}" alt="{{ $post->title }}">
            </div>
            <div class="col-12 text-center my-3">
                <h1>{{ $post->title }}</h1>
            </div>
            <div class="description">{!! $post->description !!}</div>
        </div>
    </div>
@stop
