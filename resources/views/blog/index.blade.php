@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 text-center my-3">
                <h1>Artigos</h1>
            </div>
            @forelse($posts as $post)
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="card my-3">
                        <img class="card-img-top" src="{{ $post->coverUrl() }}" alt="{{ $post->title }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ str_limit($post->title, 20) }}</h5>
                            <p class="card-text">{{ str_limit(strip_tags($post->description)) }}</p>
                            <a href="{{ route('blog.show', [ $post ]) }}" class="btn btn-primary">Veja mais</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-12 text-center">
                    <p>Nenhum artigo encontrado.</p>
                </div>
            @endforelse
            <div class="col-12 justify-content-center">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@stop
