@extends('adminlte::page')

@section('title', 'NetzeeBlog - Novo Artigo')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Novo Artigo</h3>
        </div>
        <form role="form" action="{{ route('dashboard.posts.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Título" value="{{ old('title') }}">
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control select2" id="status">
                        <option value="1" selected>Ativo</option>
                        <option value="0">Inativo</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cover">Imagem de capa</label>
                    <input type="file" name="cover" id="cover">
                </div>
                <div class="form-group">
                    <label for="categories">Categorias</label>
                    <select name="categories[]" class="form-control select2" id="categories" multiple>
                        @foreach($categories as $category)
                            <option
                                value="{{ $category->id }}"
                                {{ in_array($category->id, old('categories', [])) ? 'selected' : '' }}>
                                {{ $category->title }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control editor" name="description">{!! old('description') !!}</textarea>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
@stop
