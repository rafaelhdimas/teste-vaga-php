@extends('adminlte::page')

@section('title', 'NetzeeBlog - Artigos')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Artigos</h3>

            <div class="box-tools">
                <form action="{{ route('dashboard.posts.index') }}">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            <a href="{{ url()->current() }}" type="reset" class="btn btn-warning"><i class="fa fa-ban"></i></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Descrição</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($posts as $post)
                        <tr data-href="{{ route('dashboard.posts.edit', [$post]) }}" style="cursor: pointer;">
                            <td>{{ str_limit($post->title, 50, '...') }}</td>
                            <td>{{ str_limit(strip_tags($post->description), 100, '...') }}</td>
                            <td>
                                <span class="label label-{{ $post->status ? 'success' : 'danger' }}">
                                    {{ $post->status ? 'Ativo' : 'Inativo' }}
                                </span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" align="center">Nenhum registro encontrado</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>

        <div class="box-footer clearfix">
            <div class="no-margin pull-right">
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@stop
