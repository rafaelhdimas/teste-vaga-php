@extends('adminlte::page')

@section('title', 'NetzeeBlog - Editar Artigo')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Editar Artigo</h3>

            <div class="box-tools">
                <form class="delete-register" action="{{ route('dashboard.posts.destroy', [ $post ]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">
                        <i class="fa fa-remove"></i>
                        Excluir
                    </button>
                </form>
            </div>
        </div>
        <form role="form" action="{{ route('dashboard.posts.update', [ $post ]) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Título" value="{{ $post->title }}">
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control select2" id="status">
                        <option value="1" {{ $post->status === 1 ? 'selected' : '' }}>Ativo</option>
                        <option value="0" {{ $post->status === 0 ? 'selected' : '' }}>Inativo</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cover">Imagem de capa</label>
                    <input type="file" name="cover" id="cover">
                </div>
                <div class="form-group">
                    <label for="categories">Categorias</label>
                    <select name="categories[]" class="form-control select2" id="categories" multiple>
                        @foreach($categories as $category)
                            <option
                                value="{{ $category->id }}"
                                {{ in_array($category->id, $postCategoryIds) ? 'selected' : '' }}>
                                {{ $category->title }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control editor" name="description">{!! $post->description !!}</textarea>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
@stop
