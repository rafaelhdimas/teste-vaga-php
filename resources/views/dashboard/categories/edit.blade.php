@extends('adminlte::page')

@section('title', 'NetzeeBlog - Editar Categoria')

@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Editar Categoria</h3>

            <div class="box-tools">
                <form class="delete-register" action="{{ route('dashboard.categories.destroy', [ $category ]) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-sm btn-danger">
                        <i class="fa fa-remove"></i>
                        Excluir
                    </button>
                </form>
            </div>
        </div>
        <form role="form" action="{{ route('dashboard.categories.update', [ $category ]) }}" method="POST">
            @method('PUT')
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Título" value="{{ $category->title }}">
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" class="form-control select2" id="status">
                        <option value="1" {{ $category->status === 1 ? 'selected' : '' }}>Ativo</option>
                        <option value="0" {{ $category->status === 0 ? 'selected' : '' }}>Inativo</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="description">Descrição</label>
                    <textarea class="form-control" name="description">{{ $category->description }}</textarea>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
        </form>
    </div>
@stop
