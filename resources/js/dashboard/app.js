($ => {
    $(document).ready(function () {
        const $select2 = $('.select2');
        if ($select2.length) $select2.select2();

        const $editor = $('.editor');
        if ($editor.length) $editor.wysihtml5();

        const $buttonDeleteRegister = $('.delete-register');
        if ($buttonDeleteRegister.length) {
            $buttonDeleteRegister.on('submit', () => confirm('Deseja realmente excluir este registro?'));
        }

        const $elementsClickable = $('[data-href]');
        $elementsClickable.on('click', function() {
            window.location = $(this).data('href')
        });
    });
})(jQuery);
