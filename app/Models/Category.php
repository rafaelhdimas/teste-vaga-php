<?php

namespace NetzeeBlog\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [ 'title', 'status', 'description' ];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}
