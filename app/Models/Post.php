<?php

namespace NetzeeBlog\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const COVER_PATH = 'public/posts/cover';
    const COVER_PATH_URL = 'storage/posts/cover';

    protected $fillable = [ 'title', 'status', 'description', 'cover' ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function coverUrl()
    {
        return asset(self::COVER_PATH_URL.'/'.$this->cover);
    }
}
