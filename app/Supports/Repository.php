<?php

namespace NetzeeBlog\Supports;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use NetzeeBlog\Supports\Contracts\Repository as RepositoryContract;

abstract class Repository implements RepositoryContract
{
    protected $modelClass;

    /**
     * @var bool|string
     */
    protected $trashed = false;

    /**
     * @var array
     */
    protected $with = [];

    /**
     * @var mixed
     */
    protected $select;

    /**
     * @param int $take
     * @param bool $paginate
     * @return Builder|Builder[]|Collection|Model[]
     */
    public function all($take = 15, $paginate = true)
    {
        return $this->doQuery(null, $take, $paginate);
    }

    /**
     * @param mixed $id
     * @param array $columns
     * @return Builder|Builder[]|Collection|Model|Model[]|null
     */
    public function find($id, $columns = ['*'])
    {
        return $this->newQuery()->find($id, $columns);
    }

    /**
     * @param string $key
     * @param string $value
     * @return Builder|Builder[]|Collection|Model|Model[]|mixed|object|null
     */
    public function findBy($key, $value)
    {
        return $this->findWhere([$key, $value])->first();
    }

    /**
     * @param array $where
     * @return Model|Model[]|Builder|Builder[]|Collection
     */
    public function findWhere(array $where)
    {
        $model = $this->newQuery();
        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($field, $condition, $val) = $value;
                $model->where($field, $condition, $val);
            } else {
                $model->where($field, '=', $value);
            }
        }
        return $model;
    }

    /**
     * @param array $attributes
     * @return Builder|Model
     */
    public function create(array $attributes = [])
    {
        $model = $this->factory($attributes);
        $this->save($model);
        return $model;
    }
    /**
     * @param Model|mixed $model
     * @param array $values
     * @return bool|Model|int
     */
    public function update($model, array $values)
    {
        if (!($model instanceof Model)) {
            $model = $this->find($model);
        }
        $this->fill($model, $values)->save($model);
        return $model;
    }

    /**
     * @param string $key
     * @param string $value
     * @param array $values
     * @return bool|int|RepositoryContract
     */
    public function updateBy($key, $value, array $values)
    {
        return $this->findBy($key, $value)->each(function ($model) use ($values) {
            return $this->update($model, $values);
        });
    }

    public function updateOrCreate(array $values, Model $model = null)
    {
        if ($model instanceof Model) {
            return $this->update($model, $values);
        }
        return $this->create($values);
    }

    /**
     * @param array $where
     * @return bool|mixed|null
     * @throws \Exception
     */
    public function deleteWhere(array $where)
    {
        return $this->findWhere($where)->delete();
    }

    /**
     * @param string $column
     * @param string $direction
     * @return Builder
     */
    public function orderBy($column, $direction = 'asc')
    {
        return $this->newQuery()->orderBy($column, $direction);
    }

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginate($perPage = 10)
    {
        return $this->newQuery()->paginate($perPage);
    }

    /**
     * @param array $where
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginateByWhere(array $where, $perPage = 10)
    {
        return $this->findWhere($where)->paginate($perPage);
    }

    /**
     * @return Model
     */
    public function newModel()
    {
        return $this->newQuery()->getModel();
    }

    /**
     * @return Builder
     */
    protected function newQuery()
    {
        /** @var Builder $query */
        $query = app()->make($this->modelClass)->newQuery();

        if ('only' === $this->trashed) {
            $query->onlyTrashed();
        }
        if ('with' === $this->trashed) {
            $query->withTrashed();
        }
        if (!empty($this->with)) {
            $query->with($this->with);
        }
        if (!empty($this->select)) {
            $query->select($this->select);
        }
        return $query;
    }

    /**
     * @return $this
     */
    public function trashedWith()
    {
        $this->trashed = 'with';
        return $this;
    }

    /**
     * @return $this
     */
    public function trashedOnly()
    {
        $this->trashed = 'only';
        return $this;
    }

    /**
     * @param array $with
     * @return $this
     */
    public function with(array $with = [])
    {
        $this->with = $with;
        return $this;
    }

    /**
     * @param mixed $select
     * @return $this
     */
    public function select($select = null)
    {
        $this->select = $select;
        return $this;
    }

    /**
     * @param Builder|null $query
     * @param int $take
     * @param bool $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Builder[]|Collection
     */
    protected function doQuery($query = null, $take = 10, $paginate = true)
    {
        if (null === $query) {
            $query = $this->newQuery();
        }
        if (true === $paginate) {
            return $query->paginate($take);
        }
        if ($take > 0 && false !== $take) {
            $query->take($take);
        }
        return $query->get();
    }

    /**
     * @param array $data
     * @return Builder|Model
     */
    protected function factory(array $data)
    {
        $model = $this->newModel()->newInstance();
        $this->fill($model, $data);
        return $model;
    }

    /**
     * @param Model $model
     * @param array $data
     * @return $this
     */
    protected function fill(Model $model, array $data = [])
    {
        $model->fill($data);
        return $this;
    }

    /**
     * @param Model $model
     * @return bool
     */
    protected function save(Model $model)
    {
        return $model->save();
    }
}
