<?php

namespace NetzeeBlog\Supports\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface Repository
{
    /**
     * @param int $take
     * @param bool $paginate
     * @return Builder|Builder[]|Collection|Model[]
     */
    public function all($take = 15, $paginate = true);

    /**
     * @param mixed $id
     * @param array $columns
     * @return Model|Model[]|Builder|Builder[]|Collection|null
     */
    public function find($id, $columns = ['*']);

    /**
     * @param string $key
     * @param string $value
     * @return Model|Model[]|Builder|Builder[]|Collection|null
     */
    public function findBy($key, $value);

    /**
     * @param array $where
     * @return Model|Model[]|Builder|Builder[]|Collection
     */
    public function findWhere(array $where);

    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes = []);

    /**
     * @param mixed $id
     * @param array $values
     * @return Model
     */
    public function update($id, array $values);

    /**
     * @param string $key
     * @param string $value
     * @param array $values
     * @return $this
     */
    public function updateBy($key, $value, array $values);

    /**
     * @param array $where
     * @return mixed
     */
    public function deleteWhere(array $where);

    /**
     * @param string $column
     * @param string $direction
     * @return Builder
     */
    public function orderBy($column, $direction = 'asc');

    /**
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginate($perPage = 10);

    /**
     * @param int $perPage
     * @param array $where
     * @return LengthAwarePaginator
     */
    public function paginateByWhere(array $where, $perPage = 10);

    /**
     * @return Model
     */
    public function newModel();
}
