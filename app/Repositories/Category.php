<?php

namespace NetzeeBlog\Repositories;

use NetzeeBlog\Models\Category as CategoryModel;
use NetzeeBlog\Supports\Repository;

class Category extends Repository
{
    protected $modelClass = CategoryModel::class;

    /**
     * @param CategoryModel|mixed $category
     * @param array $posts
     * @return $this
     */
    public function syncPosts($category, array $posts = [])
    {
        $category = ($category instanceof $this->modelClass) ? $category : $this->find($category);
        $category->posts()->sync($posts);
        return $this;
    }
}
