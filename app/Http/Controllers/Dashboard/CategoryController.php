<?php

namespace NetzeeBlog\Http\Controllers\Dashboard;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use NetzeeBlog\Http\Requests\Category as RequestCategory;
use NetzeeBlog\Models\Category;
use NetzeeBlog\Http\Controllers\Controller;
use NetzeeBlog\Services\Category as ServiceCategory;

class CategoryController extends Controller
{
    /**
     * @var ServiceCategory
     */
    protected $serviceCategory;

    public function __construct(ServiceCategory $serviceCategory)
    {
        $this->serviceCategory = $serviceCategory;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categories = $this->serviceCategory->filterPaginate($request);
        return view('dashboard.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RequestCategory $request
     * @return Response
     */
    public function store(RequestCategory $request)
    {
        $category = $this->serviceCategory->saveCategory($request);
        return redirect()
            ->route('dashboard.categories.edit', [ 'categories' => $category ])
            ->with('success', 'Categoria salva com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category  $category
     * @return Response
     */
    public function edit(Category $category)
    {
        return view('dashboard.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RequestCategory $request
     * @param Category $category
     * @return Response
     */
    public function update(RequestCategory $request, Category $category)
    {
        $category = $this->serviceCategory->saveCategory($request, $category);
        return redirect()
            ->route('dashboard.categories.edit', [ 'categories' => $category ])
            ->with('success', 'Categoria salva com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category  $category
     * @return Response
     */
    public function destroy(Category $category)
    {
        try {
            $this->serviceCategory->deleteCategoryDetachPosts($category);
            return redirect()
                ->route('dashboard.categories.index')
                ->with('success', 'Categoria removida com sucesso.');
        } catch (\Exception $e) {
            return redirect()
                ->route('dashboard.categories.edit', [ 'categories' => $category ])
                ->withErrors([ $e->getMessage() ]);
        }
    }
}
