<?php

namespace NetzeeBlog\Http\Controllers\Dashboard;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Response;
use NetzeeBlog\Http\Controllers\Controller;
use Illuminate\Http\Request;
use NetzeeBlog\Http\Requests\Post as RequestPost;
use NetzeeBlog\Models\Post;
use NetzeeBlog\Repositories\Category;
use NetzeeBlog\Services\Post as ServicePost;

class PostController extends Controller
{
    /**
     * @var ServicePost|Builder
     */
    private $servicePost;

    /**
     * @var Category
     */
    private $category;

    public function __construct(ServicePost $post, Category $category)
    {
        $this->servicePost = $post;
        $this->category= $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $posts = $this->servicePost->filterPaginate($request);
        return view('dashboard.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = $this->category->all(false, false);
        return view('dashboard.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RequestPost $request
     * @return Response
     */
    public function store(RequestPost $request)
    {
        $post = $this->servicePost->savePostWithCategories($request);
        return redirect()
            ->route('dashboard.posts.edit', [ 'posts' => $post ])
            ->with('success', 'Artigo salvo com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return Response
     */
    public function edit(Post $post)
    {
        $categories = $this->category->all(false, false);
        $postCategoryIds = $post->categories()->allRelatedIds()->toArray();
        return view('dashboard.posts.edit', compact('post', 'categories', 'postCategoryIds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RequestPost $request
     * @param  Post $post
     * @return Response
     */
    public function update(RequestPost $request, Post $post)
    {
        $post = $this->servicePost->savePostWithCategories($request, $post);
        return redirect()
            ->route('dashboard.posts.edit', [ 'posts' => $post ])
            ->with('success', 'Artigo salvo com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post $post
     * @return Response
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        try {
            $this->servicePost->deletePostDetachCategories($post);
            return redirect()
                ->route('dashboard.posts.index')
                ->with('success', 'Artigo removido com sucesso.');
        } catch (\Exception $e) {
            return redirect()
                ->route('dashboard.posts.edit', [ 'posts' => $post ])
                ->withErrors([ $e->getMessage() ]);
        }
    }
}
