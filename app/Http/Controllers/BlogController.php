<?php

namespace NetzeeBlog\Http\Controllers;

use Illuminate\Http\Response;
use NetzeeBlog\Models\Post as ModelPost;
use NetzeeBlog\Repositories\Post as RepositoryPost;

class BlogController extends Controller
{
    /**
     * @var RepositoryPost
     */
    protected $repositoryPost;

    public function __construct(RepositoryPost $repositoryPost)
    {
        $this->repositoryPost = $repositoryPost;
    }

    /**
     * List all posts
     *
     * @return Response
     */
    public function index()
    {
        $posts = $this->repositoryPost->all();
        return view('blog.index', compact('posts'));
    }

    /**
     * @param ModelPost $post
     * @return Response
     */
    public function show(ModelPost $post)
    {
        return view('blog.show', compact('post'));
    }
}
