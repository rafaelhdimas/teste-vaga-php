<?php

namespace NetzeeBlog\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Post extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'status' => 'required',
            'description' => 'required',
            'cover' => 'image',
            'categories' => 'array',
        ];
    }
}
