<?php

namespace NetzeeBlog\Services;

use Illuminate\Http\Request;
use NetzeeBlog\Http\Requests\Category as RequestCategory;
use NetzeeBlog\Models\Category as ModelCategory;
use NetzeeBlog\Repositories\Category as RepositoryCategory;

class Category
{
    /**
     * @var RepositoryCategory
     */
    protected $repositoryCategory;

    public function __construct(RepositoryCategory $repositoryCategory)
    {
        $this->repositoryCategory = $repositoryCategory;
    }

    /**
     * @param RequestCategory $request
     * @param ModelCategory|null $category
     * @return bool|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|int
     */
    public function saveCategory(RequestCategory $request, ModelCategory $category = null)
    {
        $data = $request->validated();
        return $this->repositoryCategory->updateOrCreate($data, $category);
    }

    /**
     * @param Request $request
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filterPaginate(Request $request, $perPage = 10)
    {
        $where = [];
        if ($request->has('table_search')) {
            $where[] = [
                'title',
                'like',
                '%'.$request->get('table_search','').'%',
            ];
        }
        return $this->repositoryCategory->paginateByWhere($where, $perPage);
    }

    /**
     * @param ModelCategory $category
     * @throws \Exception
     */
    public function deleteCategoryDetachPosts(ModelCategory $category)
    {
        $this->repositoryCategory->syncPosts($category);
        $category->delete();
    }
}
