<?php

namespace NetzeeBlog\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use NetzeeBlog\Http\Requests\Post as RequestPost;
use NetzeeBlog\Models\Post as ModelPost;
use NetzeeBlog\Repositories\Post as RepositoryPost;

class Post
{
    /**
     * @var RepositoryPost
     */
    protected $repositoryPost;

    public function __construct(RepositoryPost $repositoryPost)
    {
        $this->repositoryPost = $repositoryPost;
    }

    /**
     * @param Request $request
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function filterPaginate(Request $request, $perPage = 10)
    {
        $where = [];
        if ($request->has('table_search')) {
            $where[] = [
                'title',
                'like',
                '%'.$request->get('table_search','').'%',
            ];
        }
        return $this->repositoryPost->paginateByWhere($where, $perPage);
    }

    /**
     * @param RequestPost $request
     * @param ModelPost $post
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function savePostWithCategories(RequestPost $request, ModelPost $post = null)
    {
        $data = $request->validated();
        $post = $this->repositoryPost->updateOrCreate($data, $post);
        $this->repositoryPost->syncCategories($post, array_get($data, 'categories', []));
        return $post;
    }

    public function deletePostDetachCategories(ModelPost $post)
    {
        $this->repositoryPost->syncCategories($post);
        $filePath = storage_path('app/'.ModelPost::COVER_PATH.'/'.$post->cover);
        if (File::exists($filePath)) {
            File::delete($filePath);
        }
        $post->delete();
    }
}
