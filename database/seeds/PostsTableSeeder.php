<?php

use Illuminate\Database\Seeder;
use NetzeeBlog\Models\Category;
use NetzeeBlog\Models\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();
        factory(Post::class, 20)
            ->create()
            ->each(function (Post $post) use ($categories) {
                $post->categories()->sync(
                    $categories
                        ->random(mt_rand(1, 5))
                        ->pluck('id')
                        ->toArray()
                );
            });
    }
}
