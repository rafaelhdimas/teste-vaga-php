<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\File;
use NetzeeBlog\Models\Post;

$filePath = storage_path('app/public/posts/cover');
if(!File::exists($filePath)){
    File::makeDirectory($filePath, 0755, true);
}

$factory->define(Post::class, function (Faker $faker) use ($filePath) {
    return [
        'title' => $faker->sentence,
        'status' => $faker->boolean,
        'description' => $faker->randomHtml($faker->numberBetween(5, 20)),
        'cover' => $faker->image($filePath, 900, 300, null, false),
    ];
});
