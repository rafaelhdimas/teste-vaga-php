<?php

use Faker\Generator as Faker;
use NetzeeBlog\Models\Category;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'status' => $faker->boolean,
        'description' => $faker->realText(),
    ];
});
