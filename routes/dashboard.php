<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/

Route::get('/', function () {
    return redirect(route('dashboard.posts.index'));
})->name('index');

Route::resource('posts', 'PostController')->except('show');
Route::resource('categories', 'CategoryController')->except('show');
